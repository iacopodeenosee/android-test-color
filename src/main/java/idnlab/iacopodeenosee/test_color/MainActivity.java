package idnlab.iacopodeenosee.test_color;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText edt_AC, edt_R, edt_G, edt_B;
    TextView tvw_H, tvw_I;
    Button btn_Apply;
    private int Red, Green, Blue, AlphaChannel = 0;
    private int COLOR = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edt_AC = findViewById(R.id.edt_alphachannel);
        edt_R = findViewById(R.id.edt_red);
        edt_G = findViewById(R.id.edt_green);
        edt_B = findViewById(R.id.edt_blue);

        tvw_H = findViewById(R.id.tvw_Hex);
        tvw_I = findViewById(R.id.tvw_Int);

        btn_Apply = findViewById(R.id.btn_apply);
        btn_Apply.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                AlphaChannel = Integer.parseInt(edt_AC.getText().toString());
                Red = Integer.parseInt(edt_R.getText().toString());
                Green = Integer.parseInt(edt_G.getText().toString());
                Blue = Integer.parseInt(edt_B.getText().toString());

                COLOR = Color.argb(AlphaChannel, Red, Green, Blue);


                setBackground(COLOR);

                tvw_I.setText(String.valueOf(COLOR));
                tvw_H.setText(Integer.toHexString(COLOR));
            }
        });
    }

    private void setBackground(int color) {
        View view = this.getWindow().getDecorView();
        view.setBackgroundColor(color);
    }
}
